import React from 'react';
import PokemonsWrapper  from '../components/Wrapper/Wrapper';
import Header from '../components/Header/Header';
import Hero from '../components/Hero/Hero';

export default function Main() {
  return (
    <div>
      <Header />
      <Hero />
      <PokemonsWrapper />
    </div>
  )
}
