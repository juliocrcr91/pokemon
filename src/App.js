import React from "react"
import AOS from 'aos';
import 'aos/dist/aos.css'; 
import Main from "./pages/Main";
import Footer from "./components/Footer/Footer";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

export default function App() {
  AOS.init();
  return (
    <div className="App">
      <Main />
      <Footer />
    </div>
  );
}

