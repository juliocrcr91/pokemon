import { faBullseye, faHome, faListDots } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState } from 'react'
import { Navbar, Container, Nav, Image } from 'react-bootstrap'
import { Link } from "react-scroll"
import logo from "../../assets/Images/logo.png"
import "./Header.scss"

export default function Header() {
  const [navbar, setNavbar] = useState(false);

  const changeBackground = () => {
    if (window.scrollY >= 80) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  }

  window.addEventListener('scroll', changeBackground);

  return (
    <div >
      <Navbar fixed="top" variant="dark" expand="lg" className={navbar ? "navbar active" : "navbar"}>
        <Container fluid>
          <Navbar.Brand href="/"><Image src={logo} alt="logotipo" width={50} /></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto" navbarScroll>
              <Link to="hero" className='text-warning text-decoration-none me-2 pointer' ><h5> <FontAwesomeIcon icon={faHome} className="me-2" />Home</h5></Link>
              <Link to="categorias" className='text-warning text-decoration-none me-2 pointer' ><h5><FontAwesomeIcon icon={faListDots} className="me-2" />Categorias</h5></Link>
              <Link to="pokemons" className='text-warning text-decoration-none me-2 pointer' ><h5><FontAwesomeIcon icon={faBullseye} className="me-2" />Pokemons</h5></Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}
