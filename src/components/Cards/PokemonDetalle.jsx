import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import { Container, Row, Col, Button, Image, Card } from 'react-bootstrap';
import "./PokemonDetalle.scss"

export default function PokemonDetalle({ item, close }) {

  console.log('item :>> ', item);
  return (
    <div className='detalle' name="pokemons">
      <Container className='pb-5 ' >
        <div className='d-flex justify-content-start'>
          <Button onClick={close} variant="warning" className='shadow zoom' > <FontAwesomeIcon icon={faArrowLeft} className="me-2" />Volver</Button>
        </div>

        <h2 className=' bold'>#00{item.id}</h2>
        <h1 className='mb-5'>{item.name.toUpperCase()}</h1>
        <Row className='justify-content-center align-items-center'>
          <Col xs={12} md={6} className="mb-4">
            <div>
              <Image fluid src={item.sprites.other.dream_world.front_default} alt="pokemons" />
            </div>
          </Col>
          <Col xs={12} md={6}>
            <Card className='py-4 bgcard'>
              <h5 className='mb-2'>Type </h5>
              <Row className='justify-content-center align-items-center'>
                {item.types.map(({ type }, index) => (
                  <Col className='mb-2' key={index} xs={12} md={12}>
                    <h5 className={type.name}>{type.name}</h5>
                  </Col>
                ))}
              </Row>
              <Row>
                <Col xs={12} md={6}>
                  <h5 className='my-2 p-0'>Height: {item.height}</h5>
                  <h5 className='my-2 p-0'>Weight: {item.weight}</h5>
                </Col>
                <Col xs={12} md={6}>
                  <h5 className="my-3 p-0">Abilities:</h5>
                  {item.abilities.map(({ ability }, index) => <h5 key={index}>{ability.name}</h5>)}
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        <Row className=' my-5 py-5'>
          <h2 className='mb-5'>Stats</h2>
          <Col xs={6}>
            {item.stats.map(({ stat }, index) => <h5 key={index}>{stat.name}</h5>)}
          </Col>
          <Col xs={6}>
            {item.stats.map((stat, index) => <h5 key={index}>{stat.base_stat}</h5>)}

          </Col>
        </Row>

      </Container >
    </div>
  )
}
