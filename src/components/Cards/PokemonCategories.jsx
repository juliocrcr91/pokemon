import React from 'react'
import { Container, Row, Col } from "react-bootstrap"
import "./PokemonCategories.scss"

export default function PokemonCategories({ setType }) {
  const categories = ["Normal", "Fire", "Water", "Grass", "Flying", "Fighting", "Poison", "Electric", "Ground", "Rock", "Psychic", "Ice", "Bug", "Ghost", "Steel", "Dragon", "Dark", "Fairy"];

  return (
    <Container className='mb-5 pt-5 categories' name="categorias">
      <h1>Categorías</h1>
      <Row className='my-5 justify-content-center align-items-center'>
        <Col data-aos="zoom-in-up" data-aos-duration="1500" data-aos-anchor-placement="bottom-bottom" className='pointer zoom' xs={4} md={2} onClick={() => setType(null)}>
          <h4 className='dark'>All</h4>
        </Col>
        {
          categories.map(c =>
            <Col data-aos="zoom-in-up" data-aos-duration="1500" data-aos-anchor-placement="bottom-bottom" className='pointer zoom' xs={4} md={2} key={c} onClick={() => setType(c.toLowerCase())}>
              <h4 className={c.toLowerCase()}>{c}</h4>
            </Col>
          )
        }
      </Row>
    </Container>
  )
}
