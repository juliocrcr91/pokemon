import React, { useState } from 'react'
import { faArrowDown, faBookBookmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Container, Row, Col, Card, Button, Image } from 'react-bootstrap';
import PokemonDetalle from './PokemonDetalle';
import "./AllPokemon.scss"

export default function AllPokemon({ allPokemons, getAllPokemons }) {
  const [detalle, setDetalle] = useState({ show: false, item: null })

  if (detalle.show) return (<PokemonDetalle item={detalle.item} close={() => setDetalle({ show: false, item: null })} />)

  return (
    <Container className='mt-5 pokemons' name="pokemons">
      <h1 className='mb-5'>Pokedex</h1>
      <Row className='justify-content-center align-items-center'>
        {allPokemons.map(pokemon =>
          <Col  data-aos="fade-up" data-aos-duration="1500" xs={12} md={3} key={pokemon.id}>
            <Card className='my-3 border-rounded zoom shadow bgcard'>
              <Card.Body>
                <p className=' text-start bold'>ID: #00{pokemon.id}</p>
                <div>
                  <Image className='imgpokemons' src={pokemon.sprites.other.dream_world.front_default} alt="pokemons" />
                </div>
                <h4>{pokemon.name.toUpperCase()}</h4>
                <h5 className='mb-4'>Type</h5>
                <Row className='justify-content-center align-items-center'> 
                {pokemon.types.map(({type}, index) => <Col key={index} xs={6}><h5 className={type.name}>{type.name}</h5></Col>) }
                </Row>
                <Button variant="warning" className='rounded-pill bold mt-4 px-5' onClick={() => setDetalle({show:true, item: pokemon})}><FontAwesomeIcon icon={faBookBookmark} className="me-2" />Info</Button>
              </Card.Body>
            </Card>
          </Col>
        )}
      </Row>
      <Button variant='warning' onClick={() => getAllPokemons()} className='rounded-circle bold my-5'>
        <FontAwesomeIcon icon={faArrowDown} className="fa-lg" />
      </Button>

    </Container >
  )
}
