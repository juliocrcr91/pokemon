import React, { useState, useEffect } from 'react';
import AllPokemon from '../Cards/AllPokemon';
import PokemonCategories from '../Cards/PokemonCategories';
import Search from '../Search/Search';
import './Wrapper.scss';

export default function Wrapper() {
  const [loadMore, setLoadMore] = useState('https://pokeapi.co/api/v2/pokemon?limit=100'),
  [allPokemons, setAllPokemons] = useState([]),
  [filtered, setFiltered] = useState([]),
  [type, setType] = useState(null),

  getAllPokemons = async () => {
    const res = await fetch(loadMore);
    const data = await res.json();

    setLoadMore(data.next);

    function createPokemonObject(results) {
      results.forEach(async pokemon => {
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`);
        const data = await res.json();
        setAllPokemons(currentList => [...currentList, data]);
        allPokemons.sort((a, b) => a.id - b.id);
      });
    }
    createPokemonObject(data.results);
  };

  useEffect(() => getAllPokemons(), []); // eslint-disable-line

  return (
    <div className='wrapper'> 
      <PokemonCategories setType={setType} />
      <Search allPokemons={allPokemons} type={type} setFiltered={setFiltered} />
      <AllPokemon allPokemons={filtered} getAllPokemons={getAllPokemons} />
    </div>
  );
}