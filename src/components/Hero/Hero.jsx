import React from "react";
import { Image } from "react-bootstrap";
import pokemon from "../../assets/Images/pokemon.png"
import "./Hero.scss";

export default function Hero() {
  return (
    <div className="hero" name="hero">
      <div className="hero-heroimg">
        <Image src={pokemon} alt="logo" className="logoimg" />
      </div>
    </div>
  );
}
