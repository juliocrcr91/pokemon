import React from 'react';
import { Container, Row } from 'react-bootstrap';
import './Footer.scss'

export default function Footer() {
  return (
    <div className="footer">
      <Container className="ptft">
        <Row className="flex-column align-items-center justify-content-center">
            <p className="text-white">© Copyright <span className="red">Pokemon</span> All Rights Reserved</p>
            <p className="text-white">Designed by <span className="red">Julio Cesar Ruiz Perez De Nucci</span> </p>
        </Row>
      </Container>
    </div>
  )
}
