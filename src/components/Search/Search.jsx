import React, { useState, useEffect } from 'react';
import { Form, FormControl, Row, Col, Container } from "react-bootstrap";

export default function Search({ allPokemons, type, setFiltered }) {
  const [value, setValue] = useState(""),

  applyFilters = (val = value) => {
    let tmp = allPokemons;
    if (val) {
      if (type) {
        tmp = allPokemons.filter(p => {
          let found = false, name = false;
          if (p.name.toLowerCase().includes(val.toLowerCase()) || String(p.id).includes(val.toLowerCase())) name = true;
          for (let i = 0; i < p.types.length; i++) {
            if (p.types[i].type.name === type) found = true;
          }
          return found && name;
        });
      } else {
        console.log(val);
        tmp = allPokemons.filter(p => p.name.toLowerCase().includes(val.toLowerCase()) || String(p.id).includes(val.toLowerCase()));
      }
    } else if (type) {
      tmp = allPokemons.filter(p => {
        let found = false;
        for (let i = 0; i < p.types.length; i++) {
          if (p.types[i].type.name === type) found = true;
        }
        return found;
      });
    }
    setFiltered(tmp);
  },
  handleChange = ({ target }) => {
    let value = target.value.trim();
    setValue(value);
    applyFilters(value);
  };

  useEffect(() => applyFilters(), [type, allPokemons]); // eslint-disable-line

  return (
    <Container>
      <Row className="justify-content-center align-items-top">
        <Col xs={12} md={3} className="mb-4 mb-md-0">
          <h3>Selected type: <span className={type}>{type ? type : "All"}</span></h3>
        </Col>
        <Col xs={12} md={3}>
          <Form className="d-flex shadow" onSubmit={e => e.preventDefault()}>
            <FormControl
              type="search"
              placeholder="🔍 Buscar"
              aria-label="Search"
              value={value}
              onChange={handleChange}
            />
          </Form>
        </Col>
      </Row>
    </Container>
  );
}